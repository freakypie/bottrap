#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name='bottrap',
    version='0.5',
    description="Provides a honeypot for bots to digest",
    author="Danemco, LLC",
    author_email='john@velocitywebworks.com',
    url='https://bitbucket.org/freakypie/bottrap',
    packages=find_packages()
)

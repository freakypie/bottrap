
How to use bottrap

   1. Add "bottrap.middleware.BotTrapMiddleware" to your middlware, probably at the top
   2. Add a couple of honeypot views somewhere tempting in your urls.py (Maybe "^phpmyadmin/")
   3. Add a restricted section in your robots.txt, include one of your honeypot urls
   4. Add a link to the restricted honeypot, only evil robots will go there
   

